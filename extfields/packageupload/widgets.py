#!/usr/bin/env python
# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- coding: utf-8 -*-
"""
WIDE IO
Revision FILE: $Id: widgets.py 841 2009-08-23 13:01:52Z tranx $


#############################################################################################################
  copyright $Copyright$
  @version $Revision: 841 $
  @lastrevision $Date: 2009-08-23 22:01:52 +0900 (Sun, 23 Aug 2009) $
  @modifiedby $LastChangedBy: tranx $
  @lastmodified $LastChangedDate: 2009-08-23 22:01:52 +0900 (Sun, 23 Aug 2009) $
#############################################################################################################
"""
# create widgets related to jquery component for essential displays ...
import datetime
import re
import uuid

from django.db.models.fields import NOT_PROVIDED
from django.forms.widgets import Widget
from django.template import Context, loader
from django.utils.safestring import mark_safe
#from wioframework.uuid import getuuid
#from references.models import BibliographicReference, Image, LinkReference, Keyword

import  settings


def getuuid():
    return str(uuid.uuid1())


class APackageWidget(Widget):

    """
    Widget for selecting / updating a package related to an algorithm
    """

    def __init__(self, attrs={}):
        from extfields.fileupload.widgets import AFileWidget
        self.attrs = attrs
        self.uuid = getuuid()
        self.request = None
        from django.forms.fields import URLField
        self._git_url = URLField().widget
        self._zip_file = AFileWidget()

    def set_request(self, r):
        self.request = r
        self._zip_file.set_request(r)

    def get_prologue(self):
        return self._zip_file.get_prologue()

    def render(self, name, ovalue, attrs=None):
        widgettemplate = loader.get_template('extfields/package.html')
        return widgettemplate.render(Context({'giturl_widget': self._git_url.render(name + "__" + "git", None),
                                              'zipfile_widget': self._zip_file.render(name + "__" + "zip", None),
                                              'name': name,
                                              'value': ovalue,
                                              'MEDIA_URL': settings.MEDIA_URL
                                              }))

    def value_from_datadict(self, data, files, name):
        from software.models import PackageBranch, Package
        print "data", data, "files", files, "name", name
        if (name in data):
            return data[name]
        if ('name' in data):
            packagebranch = PackageBranch()
            packagebranch.author = self.request.user
            packagebranch.name = data['name']
            packagebranch.save()
            packagebranch.on_add(self.request)
            package = Package()
            packagebranch.package = packagebranch
            # versionid=VersionID()
            # versionid.major=0
            # versionid.minor=1
            # versionid.micro=0
            # versionid.tag="alpha"
            package.version = [0, 0, 1, "alpha"]
            package.giturl = data.get(name + "__git", None)
            package.zipfile_id = data.get(name + "__zip", None)
            package.save()
            package.on_add(self.request)
            return package.id
